module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    sass: {
      css_dist: {
        options: {
          sourcemap: 'none',
          style: 'compressed'
        },
        files: {
          'assets/dist/css/landingpage.min.css': 'assets/src/scss/landingpage.scss'
        }
      }
    },
    copy: {
      font_awesome: {
        files: [
          {
            expand: true,
            filter: 'isFile',
            flatten: true,
            cwd: 'bower_components/fontawesome/web-fonts-with-css/webfonts',
            src: ['fa-brands-400.*', 'fa-solid-900.*'],
            dest: 'assets/dist/webfonts'
          }
        ]
      }
    },
    watch: {
      sass: {
        files: ['assets/src/scss/*.scss'],
        tasks: ['sass:css_dist'],
        options: {
          livereload: true
        }
      },
      html: {
        files: ['*.html'],
        options: {
          livereload: true
        }
      },
      grunt: {
        files: ['Gruntfile.js'],
        tasks: ['build'],
        options: {
          livereload: true
        }
      }
    }
  });

  // Load plugins
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Define tasks
  grunt.registerTask('build', ['sass','copy']);
  grunt.registerTask('default', ['build','watch']);

};
